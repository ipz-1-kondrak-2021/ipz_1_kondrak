﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace hostel
{
    public partial class ComendantForm : Form
    {
        bool showBookingFlag = false;
        public ComendantForm(string email)
        {
            InitializeComponent();
            labelEmail.Text = email;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {

            DialogResult dialogresult = MessageBox.Show(
               "Ви справді бажаєте вийти?",
                "Повідомлення",
               MessageBoxButtons.YesNo,
               MessageBoxIcon.Information,
                MessageBoxDefaultButton.Button1,
                MessageBoxOptions.DefaultDesktopOnly);
            Console.Write(dialogresult);
            if (dialogresult == DialogResult.Yes)
            {
                ServerRequest server = new ServerRequest();
                string data = server.logOut(labelEmail.Text);
                if (data.Split(',')[0] == "true")
                {
                    this.Close();
                    MessageBox.Show(data.Split(',')[1]);
                }
                else
                {
                    MessageBox.Show(data.Split(',')[1]);
                }
            }
        }

        private void btnAllow_Click(object sender, EventArgs e)
        {
            if(dataGridView1.SelectedRows.Count != 0 && showBookingFlag)
            {
                string studentEmail = dataGridView1.SelectedRows[0].Cells[3].Value.ToString();
                string room = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();
                ServerRequest server = new ServerRequest();
                string data = server.allowBooking(studentEmail, room);
                if (data.Split(',')[0] == "true")
                {
                    dataGridView1.Rows.Clear();
                    MessageBox.Show(data.Split(',')[1]);
                }
                else
                {
                    MessageBox.Show(data.Split(',')[1]);
                }
            }
            else
            {
                MessageBox.Show("Спершу виконайте пошук заяв на поселення і виберіть студента з списку");
            }
        }

        private void ComendantForm_FormClosing(object sender, FormClosingEventArgs e)
        {

            DialogResult dialogresult = MessageBox.Show(
               "Ви справді бажаєте вийти?",
                "Повідомлення",
               MessageBoxButtons.YesNo,
               MessageBoxIcon.Information,
                MessageBoxDefaultButton.Button1,
                MessageBoxOptions.DefaultDesktopOnly);
            Console.Write(dialogresult);
            if (dialogresult == DialogResult.Yes)
            {
                ServerRequest server = new ServerRequest();
                string data = server.logOut(labelEmail.Text);
                if(data.Split(',')[0] == "true")
                {
                   
                    MessageBox.Show(data.Split(',')[1]);
                }
                else
                {
                    MessageBox.Show(data.Split(',')[1]);
                }
                
            }
        }

        private void btnShowRequest_Click(object sender, EventArgs e)
        {
            ServerRequest server = new ServerRequest();
            string data = server.getAllBookingRooms();
            if (data.Split(',')[0] == "true")
            {
                showBookingFlag = true;
                dataGridView1.Rows.Clear();
                string students = data.Split(',')[1];
                if (students.Split('\n').Length != 0)
                {
                    foreach (var student in students.Split('\n'))
                    {
                        if (student != "")
                        {
                            dataGridView1.Rows.Add(student.Split(' '));
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show(data.Split(',')[1]);
            }
        }

        private void btnShowAll_Click(object sender, EventArgs e)
        {
            ServerRequest server = new ServerRequest();
            string data = server.getAllRooms();
            if (data.Split(',')[0] == "true")
            {
                dataGridView1.Rows.Clear();
                string students = data.Split(',')[1];
                if (students.Split('\n').Length != 0)
                {
                    foreach (var student in students.Split('\n'))
                    {
                        if (student != "")
                        {
                            dataGridView1.Rows.Add(student.Split(' '));
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show(data.Split(',')[1]);
            }
        }
    }
}
