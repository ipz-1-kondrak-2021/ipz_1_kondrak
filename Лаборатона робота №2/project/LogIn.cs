﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Collections.Specialized;

namespace hostel
{
    public partial class LogIn : Form
    {
        public LogIn()
        {
            InitializeComponent();
        }

        private void btnLogIn_Click(object sender, EventArgs e)
        {
            if(inputEmail.Text == "" || inputPassword.Text == "")
            {
                MessageBox.Show("Заповніть всі поля");
            }
            else if (!inputEmail.Text.Contains("@lpnu.ua"))
            {
                MessageBox.Show("Пошта введено не коректно");
            }
            else
            {
                ServerRequest server = new ServerRequest();
                string data = server.logIn(inputEmail.Text.Trim(), inputPassword.Text.Trim());
                if(data.Split(',')[0] == "true")
                {
                    ComendantForm comendant = new ComendantForm(inputEmail.Text.Trim());
                    comendant.Show();
                    comendant.FormClosed += new FormClosedEventHandler(delegate { Close(); });
                    this.Hide();
                }
                else
                {
                    MessageBox.Show(data.Split(',')[1]);
                }

            }
        }

        private void inputEmail_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(inputEmail.Text, "[\u0400-\u04FF]"))
            {
                MessageBox.Show("Вводьте тільки латинські літери");
                inputEmail.Text = inputEmail.Text.Remove(inputEmail.Text.Length - 1);
            }
        }

        private void inputPassword_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(inputPassword.Text, "[^a-z0-9]"))
            {
                MessageBox.Show("Використовуйте тільки цифри або латинські літери");
                inputPassword.Text = inputPassword.Text.Remove(inputPassword.Text.Length - 1);
            }
        }

        private void btnSignUpLink_Click(object sender, EventArgs e)
        {
            SignUp signUp = new SignUp();
            signUp.Show();
            signUp.FormClosed += new FormClosedEventHandler(delegate { Close(); });
            this.Hide();
        }
    }
}
