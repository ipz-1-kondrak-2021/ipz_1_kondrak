﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace hostel
{
    public partial class SignUp : Form
    {
        public SignUp()
        {
            InitializeComponent();
        }

        private void inputEmail_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(inputEmail.Text, "[\u0400-\u04FF]"))
            {
                MessageBox.Show("Вводьте тільки латинські літери");
                inputEmail.Text = inputEmail.Text.Remove(inputEmail.Text.Length - 1);
            }
        }

        private void inputKey_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(inputKey.Text, "[^0-9]"))
            {
                MessageBox.Show("Вводьте тільки цифри");
                inputKey.Text = inputEmail.Text.Remove(inputKey.Text.Length - 1);
            }
        }

        private void btnSignUp_Click(object sender, EventArgs e)
        {
            string emailValue = inputEmail.Text.Trim();
            string passwordValue = inputPassword.Text.Trim();
            string repasswordValue = inputRepassword.Text.Trim();
            string keyValue = inputKey.Text.Trim();
            if(emailValue.Length == 0 || passwordValue.Length == 0 || repasswordValue.Length == 0 || keyValue.Length == 0)
            {
                MessageBox.Show("Заповніть усі поля");
            }
            else if (!emailValue.Contains("@lpnu.ua"))
            {
                MessageBox.Show("Пошта введено не коректно");
            }
            else if (passwordValue != repasswordValue)
            {
                MessageBox.Show("Паролі не співпадають");
            }
            else
            {
                ServerRequest server = new ServerRequest();
                string data = server.signUp(emailValue, passwordValue, keyValue);
                if (data.Split(',')[0] == "true")
                {
                    MessageBox.Show(data.Split(',')[1]);
                    ComendantForm comendant = new ComendantForm(emailValue);
                    comendant.Show();
                    comendant.FormClosed += new FormClosedEventHandler(delegate { Close(); });
                    this.Hide();
                }
                else
                {
                    MessageBox.Show(data.Split(',')[1]);
                }
            }
        }
    }
}
