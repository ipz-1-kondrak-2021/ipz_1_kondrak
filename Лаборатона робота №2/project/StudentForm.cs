﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace hostel
{
    public partial class StudentForm : Form
    {
        public StudentForm()
        {
            InitializeComponent();
            ServerRequest server = new ServerRequest();
            string data = server.getFreeRooms();
            if(data.Split(',')[0] == "true")
            {
                string rooms = data.Split(',')[1];
                if (rooms.Split('\n').Length != 0)
                {
                    foreach (var room in rooms.Split('\n'))
                    {
                        if (room != "")
                        {
                            dataGridView1.Rows.Add(room.Split(' '));
                          
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show(data.Split(',')[1]);
            }
        }

        private void inputSurname_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(inputSurname.Text, "[^\u0400-\u04FF]"))
            {
                MessageBox.Show("Вводьте тільки кирилицю");
                inputSurname.Text = inputSurname.Text.Remove(inputSurname.Text.Length - 1);
            }
        }

        private void inputName_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(inputName.Text, "[^\u0400-\u04FF]"))
            {
                MessageBox.Show("Вводьте тільки кирилицю");
                inputName.Text = inputName.Text.Remove(inputName.Text.Length - 1);
            }
        }

        private void inputEmail_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(inputEmail.Text, "[\u0400-\u04FF]"))
            {
                MessageBox.Show("Вводьте тільки латинські літери");
                inputEmail.Text = inputEmail.Text.Remove(inputEmail.Text.Length - 1);
            }
        }

        private void inputCheck_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(inputCheck.Text, "[^0-9]"))
            {
                MessageBox.Show("Вводьте тільки цифри");
                inputCheck.Text = inputCheck.Text.Remove(inputCheck.Text.Length - 1);
            }
        }

        private void sendForm_Click(object sender, EventArgs e)
        {

            string surname = inputSurname.Text.Trim(); 
            string name = inputName.Text.Trim();
            string email = inputEmail.Text.Trim();
            string specialisation = inputSpec.Text.Trim();
            string check = inputCheck.Text.Trim();
            string room = labelRoom.Text.Trim();
            if(surname.Length == 0 || name.Length == 0 || email.Length == 0 || specialisation.Length == 0 || check.Length == 0)
            {
                MessageBox.Show("Заповніть всі поля");
            }
            else if (!email.Contains("@lpnu.ua"))
            {
                MessageBox.Show("Пошта введено не коректно");
            }
            else if (room == "не вибрана")
            {
                MessageBox.Show("Виберіть кімнату з списку");
            }
            else
            {
                ServerRequest server = new ServerRequest();
                string data = server.bookingRoom(surname, name, email, specialisation, check, room);
                if (data.Split(',')[0] == "true")
                {
                    MessageBox.Show(data.Split(',')[1] + "\nПовідомлення про поселення в гуртожиток прийде вам на пошту");
                    this.Close();
                }
                else
                {
                    MessageBox.Show(data.Split(',')[1]);
                }
            }
        }

        private void btnSelectRoom_Click(object sender, EventArgs e)
        {
            if(dataGridView1.SelectedRows.Count != 0)
            {
                labelRoom.Text = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
            }
        }
    }
}
