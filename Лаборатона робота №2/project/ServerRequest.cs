﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Collections.Specialized;

namespace hostel
{
    class ServerRequest
    {
        string url = "http://localhost:3000";
        string serverEroor = "false,Сервер не відповідає\nСпробуйте пізніше";

        public class MyWebClient : WebClient
        {
            protected override WebRequest GetWebRequest(Uri uri)
            {
                WebRequest w = base.GetWebRequest(uri);
                w.Timeout = 3 * 1000;
                return w;
            }
        }

        public string logIn(string email, string password)
        {
            try
            {
                using (var webClient = new MyWebClient())
                {
                    var pars = new NameValueCollection();
                    pars.Add("data", "logIn " + email + " " + password);
                    var response = webClient.UploadValues(url, pars);
                    string res = System.Text.Encoding.UTF8.GetString(response);
                    return res;
                }
            }
            catch (WebException ex)
            {
                return serverEroor;
            }
        }

        public string signUp(string email, string password, string key)
        {
            try
            {
                using (var webClient = new MyWebClient())
                {
                    var pars = new NameValueCollection();
                    pars.Add("data", "signUp " + email + " " + password + " " + key);
                    var response = webClient.UploadValues(url, pars);
                    string res = System.Text.Encoding.UTF8.GetString(response);
                    return res;
                }
            }
            catch (WebException ex)
            {
                return serverEroor;
            }
        }


        public string getFreeRooms()
        {
            try
            {
                using (var webClient = new MyWebClient())
                {
                    var pars = new NameValueCollection();
                    pars.Add("data", "getFreeRooms" );
                    var response = webClient.UploadValues(url, pars);
                    string res = System.Text.Encoding.UTF8.GetString(response);
                    return res;
                }
            }
            catch (WebException ex)
            {
                return serverEroor;
            }
        }

        public string bookingRoom(string surname, string name, string email, string spec, string check, string room)
        {
            try
            {
                using (var webClient = new MyWebClient())
                {
                    var pars = new NameValueCollection();
                    pars.Add("data", "bookingRoom " + surname + " " + name + " " + email + " " + spec + " " + check + " " + room);
                    var response = webClient.UploadValues(url, pars);
                    string res = System.Text.Encoding.UTF8.GetString(response);
                    return res;
                }
            }
            catch (WebException ex)
            {
                return serverEroor;
            }
        }

        public string getAllBookingRooms()
        {
            try
            {
                using (var webClient = new MyWebClient())
                {
                    var pars = new NameValueCollection();
                    pars.Add("data", "getAllBookingRooms");
                    var response = webClient.UploadValues(url, pars);
                    string res = System.Text.Encoding.UTF8.GetString(response);
                    return res;
                }
            }
            catch (WebException ex)
            {
                return serverEroor;
            }
        }

        public string getAllRooms()
        {
            try
            {
                using (var webClient = new MyWebClient())
                {
                    var pars = new NameValueCollection();
                    pars.Add("data", "getAllRooms");
                    var response = webClient.UploadValues(url, pars);
                    string res = System.Text.Encoding.UTF8.GetString(response);
                    return res;
                }
            }
            catch (WebException ex)
            {
                return serverEroor;
            }
        }

        public string logOut(string email)
        {
            try
            {
                using (var webClient = new MyWebClient())
                {
                    var pars = new NameValueCollection();
                    pars.Add("data", "logOut " + email);
                    var response = webClient.UploadValues(url, pars);
                    string res = System.Text.Encoding.UTF8.GetString(response);
                    return res;
                }
            }
            catch (WebException ex)
            {
                return serverEroor;
            }
        }

        public string allowBooking(string email, string room)
        {
            try
            {
                using (var webClient = new MyWebClient())
                {
                    var pars = new NameValueCollection();
                    pars.Add("data", "allowBooking " + email + " " + room);
                    var response = webClient.UploadValues(url, pars);
                    string res = System.Text.Encoding.UTF8.GetString(response);
                    return res;
                }
            }
            catch (WebException ex)
            {
                return serverEroor;
            }
        }

    }
}
