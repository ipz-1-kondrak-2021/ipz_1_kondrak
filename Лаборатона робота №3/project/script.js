import http from 'http';
const PORT = 3000;
import { database } from './firebase.js';

const server = http.createServer(function (req, res) {
    let reqBody = '';


    req.on('data', (chunk) => {
        reqBody += chunk;
    })

    req.on('end', async () => {
        let resMessage = '';
        reqBody = decodeURI(reqBody);
        let reqBodyArr = reqBody.split('+');
        console.log(reqBodyArr);

        let task = reqBodyArr[0].replace('data=', '');
        if (task === 'signUp') {
            resMessage = await signUp(reqBodyArr[1], reqBodyArr[2], reqBodyArr[3]);
        }
        else if (task === 'logIn') {
            resMessage = await logIn(reqBodyArr[1], reqBodyArr[2]);
        }
        else if (task === 'logOut') {
            resMessage = await logOut(reqBodyArr[1]);
        }
        else if (task === 'bookingRoom') {
            resMessage = await bookingRoom(reqBodyArr[1], reqBodyArr[2], reqBodyArr[3], reqBodyArr[4], reqBodyArr[5], reqBodyArr[6]);
        }
        else if (task === 'getFreeRooms') {
            resMessage = await getFreeRooms();
        }
        else if (task === 'getAllBookingRooms') {
            resMessage = await getAllBookingRooms();
        }
        else if (task === 'allowBooking') {
            resMessage = await allowBooking(reqBodyArr[1], reqBodyArr[2]);
        }
        else if (task === 'getAllRooms') {
            resMessage = await getAllRooms();
        }


        console.log(`Відповідь сервера: ${resMessage}`);
        res.end(resMessage);
    })
});

server.listen(PORT, (error) => {
    error ? console.log(error) : console.log(`Listening on ${PORT}`);
});



async function signUp(email, password, key) {
    let message = '';
    email = email.replace('%40', '@');
    email = email.replace(/\./g, '+');
    await database.ref(`comendants/${email}`).get().then(async snapshot => {
        if (snapshot.exists()) {
            message = 'false,Комендант з такою поштою вже зареєстрований!';
        }
        else {
            let usedKeyFlag = false;
            await database.ref(`comendantsKey`).get().then(snapshot => {
                snapshot.forEach(childSnapshot => {
                    if (childSnapshot.key === key) {
                        usedKeyFlag = childSnapshot.val();
                    }
                })
            })
            if (usedKeyFlag) {
                await database.ref(`comendants/${email}`).set({
                    password,
                    'session': true,
                    key
                });
                message = 'true,Реєстрація пройшла успішно!';
            }
            else {
                message = 'false,Цей не є ваш код коменданта!';
            }
        }
    }).catch((error) => {
        console.error(error);
        message = 'false,Помилка з базою даних';
    });
    return message;
}

async function logIn(email, password) {
    let message = '';
    email = email.replace('%40', '@').replace(/\./g, '+');
    console.log(email);
    await database.ref().child(`comendants/${email}`).get().then((snapshot) => {
        if (snapshot.exists()) {
            const user = snapshot.val();
            if (password !== user.password) {
                message = 'false,Невірний пароль';
            }
            else if (user.session) {
                message = 'false,Ви не можете ввійти оскільки\комендант з таким логіном зараз перебуває у системі';
            }
            else {
                let updates = {};
                updates['session'] = true;
                database.ref().child(`comendants/${email}`).update(updates);
                message = `true,${email}`;
            }
        } else {
            message = 'false,Невірна пошта';
        }
    }).catch((error) => {
        console.error(error);
        message = 'false,Помилка з базою даних';
    });
    return message;
}

async function logOut(email) {
    let message = '';
    email = email.replace('%40', '@').replace(/\./g, '+');

    if (email) {
        await database.ref(`comendants/${email}`).get().then(async snapshot => {
            if (snapshot.exists()) {
                let updates = {};
                updates['session'] = false;
                await database.ref(`comendants/${email}`).update(updates);
                message = 'true,Можете виконати вхід';
            }
        }).catch(error => {
            message = 'false,Помилка з базою даних';
            console.log(error);
        })
    }

    return message;
}

async function bookingRoom(surname, name, email, specialisation, check, room) {
    let message = '';
    email = email.replace('%40', '@').replace(/\./g, '+');
    let updates = {};
    let student = {
        surname,
        name,
        specialisation,
        check,
    };
    await database.ref(`bookingRooms`).get().then(async snapshot => {
        if (snapshot.exists()) {
            updates[`/${room}/${email}`] = student;
            await database.ref(`bookingRooms`).update(updates);
            message = 'true,Заявку подано';
        }
        else {
            updates[`${room}/${email}`] = student;
            await database.ref(`bookingRooms`).update(updates);
            message = 'true,Заявку подано';
        }
    }).catch(error => {
        message = 'false,Помилка з базою даних';
        console.log(error);
    })
    console.log(324324);
    return message;
}

async function getFreeRooms() {
    let message = 'true,';
    await database.ref(`allRooms`).get().then(snapshot => {
        snapshot.forEach(room => {
            if (Object.keys(room.val()).length !== 4) {
                message += `${room.key} ${4 - Object.keys(room.val()).length}\n`;
            }
        })
    }).catch(error => {
        message = 'false,Помилка з базою даних';
        console.log(error);
    })
    return message;
}


async function getAllBookingRooms() {
    let message = 'true,';
    await database.ref(`bookingRooms`).get().then(snapshot => {
        snapshot.forEach(room => {
            room.forEach(student => {
                message += `${student.val().surname} ${student.val().name} ${room.key} ${student.key.replace(/\+/g, '.')}\n`;
            })
        })
    }).catch(error => {
        message = 'false,Помилка з базою даних';
        console.log(error);
    })
    return message;
}



async function getAllRooms() {
    let message = 'true,';
    await database.ref(`allRooms`).get().then(snapshot => {
        snapshot.forEach(room => {
            room.forEach(student => {
                message += `${student.val().surname} ${student.val().name} ${room.key} ${student.key.replace(/\+/g, '.')}\n`;
            })
        })
    }).catch(error => {
        message = 'false,Помилка з базою даних';
        console.log(error);
    })
    return message;
}


async function allowBooking(email, room) {
    let studentObj = {};
    let message = '';
    email = email.replace('%40', '@').replace(/\./g, '+');
    await database.ref(`bookingRooms/${room}/${email}`).get().then(snapshot => {
        if (snapshot.exists()) {
            studentObj = snapshot.val();
        }
    });
    await database.ref(`bookingRooms/${room}/${email}`).remove();

    console.log(studentObj);
    if (!message) {
        await database.ref(`allRooms/${room}`).get().then(snapshot => {
            if (snapshot.exists()) {
                let updates = {};
                updates[`${email}`] = studentObj;
                database.ref(`allRooms/${room}`).update(updates);
                message = 'true,Заявку на поселення успішно прийнято';
            }
        });
    }
    return message;
}



